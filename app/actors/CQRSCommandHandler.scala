package actors

import akka.actor.{ActorLogging, Props}
import akka.persistence._
import play.api.Configuration
import play.api.libs.ws.WSClient

class CQRSCommandHandler(config: Configuration, ws: WSClient) extends PersistentActor with ActorLogging {
  override def persistenceId: String = "CQRSCommandHandler"

  override def receiveRecover: Receive = {
    case RecoveryCompleted =>
      log.info("Recovery Complete")
    case evn: Event =>
      handleEvent(evn)
  }

  override def receiveCommand: Receive = {
    case RegisterUser(phoneNumber, username) =>
      persist(UserRegistered(phoneNumber, username))(handleEvent)
    case command: Command =>
      context.child(command.phoneNumber).map { reference =>
        reference forward command
      } getOrElse {
        sender() ! "User unknown"
      }
  }

  def handleEvent(event: Event): Unit = event match {
    case registered @ UserRegistered(phoneNumber, username, _) =>
      context.actorOf(
        props = Props(classOf[ClientCommandHandler], phoneNumber, username, config, ws),
        name = phoneNumber
      )
      if (recoveryFinished) {
        sender() ! registered
        context.system.eventStream.publish(registered)
      }
  }
}
