package actors

import akka.actor.{Actor, ActorLogging, Props}
import helpers.Database
import generated.Tables._
import org.jooq.util.postgres.PostgresDataType
import org.jooq.impl.DSL._
import akka.pattern.pipe

import scala.concurrent.Future
import scala.util.control.NonFatal

class CQRSQueryHandler(database: Database) extends Actor with ActorLogging {
  implicit val ec = context.dispatcher

  override def receive: Receive = {
    case MentionsToday(phoneNumber) =>
      countMentions(phoneNumber).map { count =>
        DailyMentionedCounts(count)
      } recover { case NonFatal(t) =>
        log.error("can't get daily mentioned count: {}", t)
        QueryFailed
      } pipeTo sender()
  }

  def countMentions(phoneNumber: String): Future[Int] = {
    database.query { sql =>
      sql.selectCount().from(MENTIONS).where(
        MENTIONS.CREATED_ON.greaterOrEqual(currentDate().cast(PostgresDataType.TIMESTAMP))
        .and(MENTIONS.USER_ID.equal(
          sql.select((TWITTER_USER.ID))
            .from(TWITTER_USER)
            .where(TWITTER_USER.PHONE_NUMBER.equal(phoneNumber))
        ))
      ).fetchOne().value1()
    }
  }
}

object CQRSQueryHandler {
  def props(database: Database) = Props(classOf[CQRSEventHandler], database)
}
