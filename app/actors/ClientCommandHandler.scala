package actors

import java.util.Locale

import akka.actor.{ActorLogging, ActorRef, Cancellable, Props}
import akka.persistence.{PersistentActor, RecoveryCompleted}
import akka.pattern.pipe
import helpers.TwitterCredentials
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.Configuration
import play.api.libs.json.JsArray
import play.api.libs.oauth.{ConsumerKey, OAuthCalculator, RequestToken}
import play.api.libs.ws.WSClient

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.util.control.NonFatal

class ClientCommandHandler(phoneNumber: String, username: String, override val config: Configuration, ws: WSClient)
  extends PersistentActor with ActorLogging with TwitterCredentials {

  override def persistenceId: String = phoneNumber

  implicit val ec = context.dispatcher

  var subscriptionScheduler: Option[Cancellable] = None
  var lastCheckMentionTime: Option[DateTime] = None
  var subscribedSMSHandler: Option[ActorRef] = None
  var unacknowledgedMentions = List.empty[MentionReceived]

  override def receiveRecover: Receive = {
    case RecoveryCompleted =>
      log.info("Recovered complete")
    case event: Event =>
      handleEvent(event)
  }

  override def postStop(): Unit = {
    subscriptionScheduler.foreach(_.cancel())
  }

  override def receiveCommand: Receive = {
    case SubscribeMentions(_) =>
      if (subscriptionScheduler.isDefined) {
        sender() ! InvalidCommand("Already subscribed to mentions")
      } else {
        persist(MentionsSubscribed())(handleEvent)
      }
    case CheckMentions => {
      val maybeMentions = for {
        (consumerKey, requestToken) <- credentials
        time <- lastCheckMentionTime
      } yield fetchMentions(consumerKey, requestToken, username, time)

      maybeMentions.foreach { mentions =>
        mentions.map { m =>
          Mentions(m)
        } recover { case NonFatal(t) =>
          log.error(t, "Could not fetch mentions")
          Mentions(Seq.empty)
        } pipeTo self
      }
    }
    case Mentions(mentions) =>
      val orderedMentions = mentions.sortBy(_._2.getMillis())
      orderedMentions.foreach { mention =>
        persist(MentionReceived(mention._1, mention._2, mention._3, mention._4))(handleEvent)
      }
    case AcknowledgeMention(id) =>
      persist(MentionAcknowledged(id))(handleEvent)
    case ConnectUser(_) =>
      subscribedSMSHandler = Some(sender())
      unacknowledgedMentions.foreach { mention =>
        subscribedSMSHandler.foreach { sender =>
          sender ! mention
        }
      }
  }

  def handleEvent(event: Event): Unit = event match {
    case subscribed @ MentionsSubscribed(timestamp) =>
      subscribedSMSHandler = Some(sender())
      lastCheckMentionTime = Some(timestamp)
      subscriptionScheduler = Some(context.system.scheduler.schedule(
        initialDelay = 10.seconds,
        interval = 60.seconds,
        receiver = self,
        message = CheckMentions
      )(context.dispatcher))
      if (recoveryFinished) {
        sender() ! subscribed
        context.system.eventStream.publish(ClientEvent(phoneNumber, username, subscribed))
      }
    case received @ MentionReceived(id, createdAt, from, text, _) =>
      lastCheckMentionTime = Some(createdAt)
      unacknowledgedMentions = received :: unacknowledgedMentions
      if (recoveryFinished) {
        subscribedSMSHandler.foreach { sender =>
          sender ! received
        }
        context.system.eventStream.publish(ClientEvent(phoneNumber, username, received))
      }
    case MentionAcknowledged(id, _) =>
      log.info("ACK from message id: {}", id)
      unacknowledgedMentions = unacknowledgedMentions.filter(_.id == id)
  }

  def fetchMentions(consumerKey: ConsumerKey,
                    requestToken: RequestToken,
                    username: String,
                    time: DateTime): Future[Seq[(String, DateTime, String, String)]] = {
    val df = DateTimeFormat.forPattern("EEE MMM dd HH:mm:ss Z yyyy").withLocale(Locale.ENGLISH)
    ws
      .url("https://api.twitter.com/1.1/search/tweets.json")
      .sign(OAuthCalculator(consumerKey, requestToken))
      .withQueryString("q" -> s"@$username")
      .get()
      .map { response =>
        val mentions = (response.json \ "statuses").as[JsArray].value.map { status =>
          val id = (status \ "id_str").as[String]
          val text = (status \ "text").as[String]
          val from = (status \ "user" \ "screen_name").as[String]
          val created_at = df.parseDateTime((status \ "created_at").as[String])

          (id, created_at, from, text)
        }
        mentions.filter(_._2.isAfter(time))
      }
  }
}

object CheckMentions
case class Mentions(mentions: Seq[(String,DateTime, String, String)])