package actors

import org.joda.time.DateTime

trait Command {
  val phoneNumber: String
}

trait Event {
  val timestamp: DateTime
}

case class RegisterUser(phoneNumber: String, username: String) extends Command
case class UserRegistered(phoneNumber: String, username: String, timestamp: DateTime = DateTime.now) extends Event
case class InvalidCommand(reason: String)
case class SubscribeMentions(phoneNumber: String) extends Command
case class MentionsSubscribed(timestamp: DateTime = DateTime.now) extends Event
case class MentionReceived(id: String, createdAt: DateTime, from: String, text: String, timestamp: DateTime = DateTime.now) extends Event
case class AcknowledgeMention(id: String)
case class MentionAcknowledged(id: String, timestamp: DateTime = DateTime.now) extends Event
case class ConnectUser(phoneNumber: String) extends Command

case class ClientEvent(phoneNumber: String, username: String, event: Event, timestamp: DateTime = DateTime.now) extends Event

trait Query
trait QueryResult
case class MentionsToday(phoneNumber: String) extends Query
case class DailyMentionedCounts(count: Int) extends QueryResult
case object QueryFailed extends QueryResult