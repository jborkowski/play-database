package actors

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.io.Tcp._
import akka.util.{ByteString, Timeout}

import scala.concurrent.duration._

class SMSHandler(connection: ActorRef) extends Actor with ActorLogging {
  implicit val ec = context.dispatcher
  implicit val timeout = Timeout(2.second)

  lazy val commandHandler = context.actorSelection("akka://application/user/sms/commandHandler")

  val MessagePattern = """[\+]([0-9]*) (.*)""".r
  val RegistrationPattern = """register (.*)""".r

  override def receive: Receive = {
    case Received(data) =>
      log.info("Received message: {}", data.utf8String)
      data.utf8String.trim match {
        case MessagePattern(number, msg) =>
          handleMessage(number, msg)
        case other =>
          log.warning("Invalid message {}", other)
          sender() ! Write(ByteString("Invalid message format.\n"))
      }
    case registered: UserRegistered =>
      connection ! Write(ByteString("Registration successful\n"))
      sender() ! SubscribeMentions(registered.phoneNumber)
    case InvalidCommand(reason) =>
      connection ! Write(ByteString(reason + "\n"))
    case MentionReceived(id, createdAt, from, text, _) =>
      connection ! Write(ByteString(s"Mentioned by @$from: $text \n"))
      sender() ! AcknowledgeMention(id)
    case DailyMentionedCounts(count) =>
      connection ! Write(ByteString(s"Daily mentioned counts is: $count."))
    case PeerClosed =>
      context stop self
  }

  def handleMessage(phoneNumber: String, message: String) = message match {
    case RegistrationPattern(userName) =>
      commandHandler ! RegisterUser(phoneNumber, userName)
    case "subscribe mentions" =>
      commandHandler ! SubscribeMentions(phoneNumber)
    case "connect" =>
      commandHandler ! ConnectUser(phoneNumber)
    case "mentions today" =>
      commandHandler ! MentionsToday(phoneNumber)
  }
}
