package actors

import javax.inject.Inject

import akka.actor.{Actor, ActorLogging, Props}
import com.google.inject.AbstractModule
import helpers.Database
import play.api.Configuration
import play.api.libs.concurrent.AkkaGuiceSupport
import play.api.libs.ws.WSClient

class SMSService @Inject() (database: Database, config: Configuration, ws: WSClient) extends Actor with ActorLogging {

  override def preStart(): Unit = {
    context.actorOf(Props[SMSServer])
    context.actorOf(Props(classOf[CQRSCommandHandler], config, ws), name = "commandHandler")
    context.actorOf(CQRSEventHandler.props(database), name = "eventHandler")
    context.actorOf(CQRSQueryHandler.props(database), name = "queryHandler")
  }
  override def receive: Receive = {
    case message =>
      log.info("Received {}", message)
  }
}

class SMSServiceModule extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = bindActor[SMSService]("sms")
}
