/**
 * This class is generated by jOOQ
 */
package generated;


import generated.tables.MentionSubscriptions;
import generated.tables.Mentions;
import generated.tables.PlayEvolutions;
import generated.tables.TwitterUser;
import generated.tables.User;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in public
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.0"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

	/**
	 * The table public.mentions
	 */
	public static final Mentions MENTIONS = generated.tables.Mentions.MENTIONS;

	/**
	 * The table public.mention_subscriptions
	 */
	public static final MentionSubscriptions MENTION_SUBSCRIPTIONS = generated.tables.MentionSubscriptions.MENTION_SUBSCRIPTIONS;

	/**
	 * The table public.play_evolutions
	 */
	public static final PlayEvolutions PLAY_EVOLUTIONS = generated.tables.PlayEvolutions.PLAY_EVOLUTIONS;

	/**
	 * The table public.twitter_user
	 */
	public static final TwitterUser TWITTER_USER = generated.tables.TwitterUser.TWITTER_USER;

	/**
	 * The table public.user
	 */
	public static final User USER = generated.tables.User.USER;
}
