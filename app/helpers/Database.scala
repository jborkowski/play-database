package helpers

import javax.inject.Inject

import akka.actor.ActorSystem
import org.jooq.{DSLContext, SQLDialect}
import org.jooq.impl.DSL

import scala.concurrent.{ExecutionContext, Future}

class Database @Inject() (val db: play.api.db.Database, val system: ActorSystem) {
  val database: ExecutionContext = system.dispatchers.lookup("contexts.database")

  def query[A](block: DSLContext => A): Future[A] = Future {
    db.withConnection { connection =>
      val sql = DSL.using(connection, SQLDialect.POSTGRES_9_4)
      block(sql)
    }
  }(database)

  def withTransaction[A](block: DSLContext => A): Future[A] = Future {
    db.withTransaction{ transaction =>
      val sql = DSL.using(transaction, SQLDialect.POSTGRES_9_5)
      block(sql)
    }
  }(database)
}
