package helpers

import play.api.Configuration
import play.api.libs.oauth.{ConsumerKey, RequestToken}

trait TwitterCredentials {
  val config: Configuration

  protected def credentials: Option[(ConsumerKey, RequestToken)] = for {
    apiKey <- config.getString("twitter.apiKey")
    apiSecret <- config.getString("twitter.apiSecret")
    token <- config.getString("twitter.token")
    tokenSecret <- config.getString("twitter.tokenSecret")
  } yield (ConsumerKey(apiKey, apiSecret), RequestToken(token, tokenSecret))
}