name := "play-database"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

resolvers += "Spy repository" at "http://files.couchbase.com/maven2"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/releases"

val scalaTestV = "1.5.1"
val postgresV = "9.4-1201-jdbc41"
val jooqV = "3.7.0"
val memcachedV = "0.7.0"
val jodatimeV = "2.7"
val akkaPmongoV = "0.7.6"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  evolutions,
  cache,
  "org.scalatestplus.play" %% "scalatestplus-play" % scalaTestV % Test,
  "org.postgresql" % "postgresql" % postgresV,
  "org.jooq" % "jooq" % jooqV,
  "org.jooq" % "jooq-codegen-maven" % jooqV,
  "org.jooq" % "jooq-meta" % jooqV,
  "com.github.mumoshu" %% "play2-memcached-play24" % memcachedV,
  "joda-time" % "joda-time" % jodatimeV,
  "com.github.ironfish" %% "akka-persistence-mongo-casbah"  % akkaPmongoV % "compile"
)

routesGenerator := InjectedRoutesGenerator

val generateJOOQ = taskKey[Seq[File]]("Generate JooQ classes")

val generateJOOQTask = (baseDirectory, dependencyClasspath in Compile, runner in Compile, streams) map {
  (base, cp, r, s) =>
    toError(r.run("org.jooq.util.GenerationTool", cp.files, Array("conf/play-database.xml"), s.log))
    ((base / "app" / "generated") ** "*.scala").get
}

generateJOOQ <<= generateJOOQTask
